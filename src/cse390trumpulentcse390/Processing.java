import java.util.HashMap;


/**
 * Has the preprocessing methods.
 * @author varungoel
 *
 */
public class Processing {
	//tokenization function that breaks the sentences into words
	public static String[] tokenizer(String sentence){
		//replace all extra white spaces with one white space
		sentence = sentence.replaceAll("\\s+", " ").replaceAll("[!]"," ! ").replaceAll("\\."," . ").replaceAll("[?]"," ? ")
				.replaceAll("[,]"," , ").replaceAll("\""," \" ").replaceAll("'"," ' ");
		String[] words = sentence.split(" ");
		return words;
	}

	//helper method to place a word in the hashmap
	public static void putStringInHashMap(String toPut,HashMap<String, Double> map){
		if(!toPut.equals("{s}") && !toPut.equals("{/s}") && !toPut.equals("")){
			if(map.get(toPut) != null)
				map.put(toPut, map.get(toPut) + 1);
			else
				map.put(toPut,1.0);
		}
	}
}
