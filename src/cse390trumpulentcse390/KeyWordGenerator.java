/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cse390trumpulentcse390;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Rahul S Agasthya and Varun Goel CSE 390 - Final Project Trumpculent
 * Application
 */
public class KeyWordGenerator {
        ArrayList<String> foes = new ArrayList<String>();
        ArrayList<String> friends = new ArrayList<String>();
        ArrayList<String> hates = new ArrayList<String>();
        ArrayList<String> foesWords = new ArrayList<String>();
        ArrayList<String> PNWords = new ArrayList<String>();
        ArrayList<String> hateWords = new ArrayList<String>();

    public String generateTrumpSentence(String arg) {
        String[] foesArray = {"Hillary Clinton", "Ted Cruz", "Bernie Sanders", "Marco Rubio", "Illegal Immigrants", "Immigrants", "Democrats", "Mexico", "Mexicans", "Clinton", "Cruz", "Sanders", "Muslims"};
        foes.addAll(Arrays.asList(foesArray));
        String[] friendsArray = {"America", "Republicans", "United States of America", "Trump", "Donald Trump", "American People", "New York", "New Yorkers"};
        friends.addAll(Arrays.asList(friendsArray));
        String[] hatesArray = {"ISIS", "Terrorism", "Islamic Terror", "Islamic Terrorism"};
        hates.addAll(Arrays.asList(hatesArray));
        String[] foesWordArray = {"Stupid", "Crazy", "Liar", "Retard", "Goofy", "Crooked"};
        foesWords.addAll(Arrays.asList(foesWordArray));
        String[] PNWordsArray = {"Great", "Good", "Mighty"};
        PNWords.addAll(Arrays.asList(PNWordsArray));
        String[] hateWordsArray = {"Bomb", "Destroy", "Exterminate", "Wipe Out", "Free of"};
        hateWords.addAll(Arrays.asList(hateWordsArray));
       
        return modify(arg);
    }

    public String modify(String sentence) {
        
        String[] wordArray = sentence.split("\\s+");
        ArrayList<String> words = new ArrayList<String>();
        words.addAll(Arrays.asList(wordArray));
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i);
            if((word.contains("Hillary") && words.get(i+1).contains("Clinton")) || (word.contains("Donald") && words.get(i+1).contains("Trump")) || (word.contains("Ted") && words.get(i+1).contains("Cruz")) || (word.contains("Marco") && words.get(i+1).contains("Rubio")) || (word.contains("Bernie") && words.get(i+1).contains("Sanders"))) {
                String lastName = words.remove(i+1);
                words.set(i, word + " " + lastName);
            }
            else if((word.contains("Illegal") && words.get(i+1).contains("Immigrants")) || (word.contains("American") && words.get(i+1).contains("People")) || (word.contains("New") && words.get(i+1).contains("York")) || (word.contains("New") && words.get(i+1).contains("Yorkers"))) {
                String lastName = words.remove(i+1);
                words.set(i, word + " " + lastName);
            }
            else if(word.contains("the") && words.get(i+1).contains("United") && words.get(i+2).contains("States")&& words.get(i+3).contains("of") && words.get(i+4).contains("America")) {
                String lastName1 = words.remove(i+1);
                String lastName2 = words.remove(i+1);
                String lastName3 = words.remove(i+1);
                String lastName4 = words.remove(i+1);
                words.set(i, word);
                i++;
                words.add(i, lastName1 + " " + lastName2 + " " + lastName3 + " " + lastName4);
            }
            int index = foes.indexOf(words.get(i));
            if (index > -1) {
                int randomIndex = (int)(Math.random()*(foesWords.size()));
                words.add(i, foesWords.get(randomIndex));
                i++;
            }
            
            index = friends.indexOf(words.get(i));
            if (index > -1) {
                int randomIndex = (int)(Math.random()*(PNWords.size()));
                words.add(i, PNWords.get(randomIndex));
                i++;
            }
            
            index = hates.indexOf(words.get(i));
            if (index > -1) {
                int randomIndex = (int)(Math.random()*(hateWords.size()));
                words.add(i, hateWords.get(randomIndex));
                i++;
            }
        }
        String ret = words.get(0);
        for(int i=1; i<words.size(); i++) 
            ret = ret + " " + words.get(i);
        return ret;
    }
}
