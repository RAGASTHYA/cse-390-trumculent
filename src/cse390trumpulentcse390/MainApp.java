import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

import javax.json.JsonArray;
import javax.json.JsonNumber;


public class MainApp {

	public static void main(String[] args) throws IOException{

		HashMap<String, Double> tagTransitionLaplaceMap = new HashMap<String, Double>();
		HashMap<String, Double> wordFrequencyMap = new HashMap<String, Double>();

		//the array of all the bigram laplace probabilities 
		JsonArray tagTransitionLaplaceArray  = JSONMethods.loadJSONFile("transition-laplace.json").getJsonArray("Transition Laplace");
		int tagTransitionLaplaceArraySize = tagTransitionLaplaceArray.size();
		//loads the tag transition probabilities
		for(int a = 0; a < tagTransitionLaplaceArraySize; a++){
			String couple = (String) tagTransitionLaplaceArray.getJsonObject(a).keySet().toArray()[0];
			JsonNumber probability = (JsonNumber) tagTransitionLaplaceArray.getJsonObject(a).get(couple);
			tagTransitionLaplaceMap.put(couple, probability.doubleValue());
		}

		//the array of all the words with their frequencies
		JsonArray wordFrequencyArray  = JSONMethods.loadJSONFile("word-frequency.json").getJsonArray("Word Frequency");
		int wordFrequencyArraySize = wordFrequencyArray.size();
		//loads the tag transition probabilities
		for(int a = 0; a < wordFrequencyArraySize; a++){
			String couple = (String) wordFrequencyArray.getJsonObject(a).keySet().toArray()[0];
			JsonNumber probability = (JsonNumber) wordFrequencyArray.getJsonObject(a).get(couple);
			wordFrequencyMap.put(couple, probability.doubleValue());
		}

		// Command to run:                         ./runTagger.sh examples/your_sentence.txt > user_tagged_sentence.txt
		Scanner input = new Scanner(System.in);
		String sample = "";
		while(!sample.equals("exit")){
			System.out.println("GIVE AN INPUT");
			sample = input.nextLine();

			if(!sample.equals("exit")){
				File directory = new File("./ark-tweet-nlp-0.3.2/examples/");
				directory.mkdir();
				File tweetFile = new File(directory, "your_sentence" + ".txt");
				tweetFile.createNewFile();
				PrintWriter sw = new PrintWriter(tweetFile);
				sw.write(sample);
				sw.close();
				File toRead = new File("./ark-tweet-nlp-0.3.2/user_tagged_sentence.txt");

				System.out.println("Do you want to proceed?");
				String promptToMoveForwardInLife = input.nextLine();
				
				HashMap<String, String> wordTagMap = new HashMap<String, String>();

				if(promptToMoveForwardInLife.equalsIgnoreCase("yes")){
					Scanner lit = new Scanner(toRead);
					String sentence = "";
					while(lit.hasNextLine()){
						sentence += lit.nextLine();
					}
					System.out.println(sentence);
					lit.close();
					POS_Methods.performParsing(sentence, wordTagMap);
					System.out.println(wordTagMap);
				}
			}
		}
		input.close();
	}

}
