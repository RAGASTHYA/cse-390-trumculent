import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 * Uses the tweebo parser's POS tagger to assign relevant tags to the words and build the appropriate maps (Transition, Emission and General Frequency)
 * @author varungoel
 *
 */
public class SpeechTaggerBuilder {

	public static void main(String[] args) throws FileNotFoundException {
		//Tag : Frequency mpa
		HashMap<String, Double> tagsFrequencyMap = new HashMap<>();

		//word -> tag pair
		HashMap<String, String> tagMap = new HashMap<>();

		//transition maps
		HashMap<String, Double> transitionMap = new HashMap<>();

		//emission maps
		HashMap<String, Double> emissionMap = new HashMap<>();

		//transition maps with laplace probability
		HashMap<String, Double> transitionLaplaceMap = new HashMap<>();

		//transition maps with laplace probability
		HashMap<String, Double> emissionLaplace = new HashMap<>();

		File file = new File("taggedTweets.txt");

		Scanner input = new Scanner(file);
		String sentence = "";
		

		while (input.hasNextLine()) {
			sentence = input.nextLine();
			POS_Methods.performParsing(sentence,tagsFrequencyMap, tagMap, transitionMap, emissionMap);
		}
		input.close();
		System.out.println(transitionMap);

		//the transition probabilites map
		for(String transitionPair: transitionMap.keySet()){
			double laplaceProbability = Estimations.laplace(transitionMap, tagsFrequencyMap, transitionPair);
			transitionLaplaceMap.put(transitionPair, laplaceProbability);
		}

		StringWriter sw = new StringWriter();
		//BUILDING THE WORD TAG MAP
		Map<String, Object> properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
		JsonWriter jsonWriter = writerFactory.createWriter(sw);

		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		JSONMethods.generateWordFrequencyJsonArray(transitionLaplaceMap, arrayBuilder);
		JsonArray wordFrequencyArray = arrayBuilder.build();

		// THEN PUT IT ALL TOGETHER IN A JsonObject
		JsonObject dataManagerJSO = Json.createObjectBuilder()
				.add("Transition Laplace", wordFrequencyArray)
				.build();

		// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
		jsonWriter.writeObject(dataManagerJSO);
		jsonWriter.close();
		// INIT THE WRITER
		OutputStream os = new FileOutputStream("transition-laplace.json");
		JsonWriter jsonFileWriter = Json.createWriter(os);
		jsonFileWriter.writeObject(dataManagerJSO);
		String prettyPrinted = sw.toString();
		PrintWriter pw = new PrintWriter("transition-laplace.json");
		pw.write(prettyPrinted);
		pw.close();
	}


}
