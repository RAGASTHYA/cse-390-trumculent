import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;


/**
 * Builds the language model from the tweets.txt
 * @author varungoel
 *@author Rahul Agasthya
 */
public class LanguageModelBuilder {

	public static void main(String[] args) throws FileNotFoundException{
		//the Hashmmap with all the bigrams and their corresponding conditional laplace probability
		HashMap<String, Double> bigramLaplaceMap = new HashMap<String,Double>();

		//map of all bigrams with their corresponding frequency
		HashMap<String, Double> bigramMap = new HashMap<String, Double>();
		//map of all words with their correspoinding frequency
		HashMap<String, Double> wordMap = new HashMap<String, Double>();

		//all the words
		ArrayList<String> words = new ArrayList<>();
		//all the bigrams
		ArrayList<String> bigrams = new ArrayList<>();

		File tweetsFile = new File("tweets.txt");
		Scanner input = new Scanner(tweetsFile);

		String sentence = "";

		while(input.hasNextLine()){
			sentence = "{s} " + input.nextLine() + " {/s}";
			//get all the individual tokens of the word
			String[] tokens = Processing.tokenizer(sentence);

			for(int i = 1; i < tokens.length; i++){
				String[] couple = new String[2];
				couple[0] = tokens[i-1];
				couple[1] = tokens[i];
				// "<" will be used as a split between the the words of the bigram
				String bigramName = couple[0] + "<" + couple[1];

				//put the bigram in the map
				Processing.putStringInHashMap(bigramName, bigramMap);

				//add the bigram name to the list of bigrams
				if(!bigrams.contains(bigramName)){
					bigrams.add(bigramName);
				}

				//put the word in the map
				Processing.putStringInHashMap(couple[0], wordMap);

				//add the word to the list of words
				if(!words.contains(couple[0])){
					words.add(couple[0]);
				}
			}
			//put the last word in the hash map of words
			Processing.putStringInHashMap(tokens[tokens.length - 1], wordMap);

			if(!words.contains(tokens[tokens.length - 1])){
				words.add(tokens[tokens.length - 1]);
			}

		}

		input.close();

		//building the laplace probability map for all
		for(String bigram: bigrams){
			double laplaceProbability = Estimations.laplace(bigramMap, wordMap, bigram);
			bigramLaplaceMap.put(bigram,laplaceProbability);
		}


		StringWriter sw = new StringWriter();

		//BUILDING THE WORD FREQUENCY MAP
		Map<String, Object> properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
		JsonWriter jsonWriter = writerFactory.createWriter(sw);

		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		JSONMethods.generateWordFrequencyJsonArray(wordMap, arrayBuilder);
		JsonArray wordFrequencyArray = arrayBuilder.build();

		// THEN PUT IT ALL TOGETHER IN A JsonObject
		JsonObject dataManagerJSO = Json.createObjectBuilder()
				.add("Word Frequency", wordFrequencyArray)
				.build();

		// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
		jsonWriter.writeObject(dataManagerJSO);
		jsonWriter.close();
		// INIT THE WRITER
		OutputStream os = new FileOutputStream("word-frequency.json");
		JsonWriter jsonFileWriter = Json.createWriter(os);
		jsonFileWriter.writeObject(dataManagerJSO);
		String prettyPrinted = sw.toString();
		PrintWriter pw = new PrintWriter("word-frequency.json");
		pw.write(prettyPrinted);
		pw.close();

		//BUILDING THE BIGRAM FREQUENCY MAP
		sw = new StringWriter();
		properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		writerFactory = Json.createWriterFactory(properties);
		jsonWriter = writerFactory.createWriter(sw);

		arrayBuilder = Json.createArrayBuilder();
		JSONMethods.generateWordFrequencyJsonArray(bigramMap, arrayBuilder);
		JsonArray bigramFrequencyArray = arrayBuilder.build();

		// THEN PUT IT ALL TOGETHER IN A JsonObject
		dataManagerJSO = Json.createObjectBuilder()
				.add("Bigram Frequency", bigramFrequencyArray)
				.build();

		// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
		jsonWriter.writeObject(dataManagerJSO);
		jsonWriter.close();
		// INIT THE WRITER
		os = new FileOutputStream("bigram-frequency.json");
		jsonFileWriter = Json.createWriter(os);
		jsonFileWriter.writeObject(dataManagerJSO);
		prettyPrinted = sw.toString();
		pw = new PrintWriter("bigram-frequency.json");
		pw.write(prettyPrinted);
		pw.close();
		
		//BUILDING THE BIGRAM LAPLACE PROBABILITY MAP
		sw = new StringWriter();
		properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		writerFactory = Json.createWriterFactory(properties);
		jsonWriter = writerFactory.createWriter(sw);

		arrayBuilder = Json.createArrayBuilder();
		JSONMethods.generateWordFrequencyJsonArray(bigramLaplaceMap, arrayBuilder);
		JsonArray bigramLaplaceArray = arrayBuilder.build();

		// THEN PUT IT ALL TOGETHER IN A JsonObject
		dataManagerJSO = Json.createObjectBuilder()
				.add("Bigram Laplace", bigramLaplaceArray)
				.build();

		// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
		jsonWriter.writeObject(dataManagerJSO);
		jsonWriter.close();
		// INIT THE WRITER
		os = new FileOutputStream("bigram-laplace.json");
		jsonFileWriter = Json.createWriter(os);
		jsonFileWriter.writeObject(dataManagerJSO);
		prettyPrinted = sw.toString();
		pw = new PrintWriter("bigram-laplace.json");
		pw.write(prettyPrinted);
		pw.close();
		

	}

}
