import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Paging;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Uses the twitter API to collect the required tweets
 * @author varungoel
 *	@author Rahul Agasthya
 */
public class TweetCollector {

	public static void main(String[] args){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey("9ZvILIpuoRVxOMG1Wd2kqDVyS");
		cb.setOAuthConsumerSecret("BZHne1mCY7tXBdOzza5Pawu8YKSD1BDH0hodgBP42VdqhDf5eq");
		cb.setOAuthAccessToken("474750626-PSGy4tJSDYye5brOwpGK2EhqGxI2yGEtKcFHzHX0");
		cb.setOAuthAccessTokenSecret("OfxE28wm9zI6sutOpx5dXwkedbeJuGICmMOLSIvYUOvA4");

		Twitter twitter = new TwitterFactory(cb.build()).getInstance();

		int pageno = 1;
		String user = "realDonaldTrump";
		List statuses = new ArrayList();

		while (true) {

			try {

				int size = statuses.size(); 
				Paging page = new Paging(pageno++, 100);
				statuses.addAll(twitter.getUserTimeline(user, page));
				if (statuses.size() == size)
					break;
			}
			catch(TwitterException e) {

				e.printStackTrace();
			}
		}

		System.out.println("Total: "+statuses.size());
		try {
			writeTweetsToFile(statuses);
		} catch (FileNotFoundException e) {
			System.out.println("COULDN'T write to file");
		}

	}

	/**
	 * Writes the tweets to the file
	 * @param statuses is the list of all tweets
	 * @throws FileNotFoundException
	 */
	public static void writeTweetsToFile(List statuses) throws FileNotFoundException{
		File tweetsFile = new File("tweets.txt");
		PrintWriter sw = new PrintWriter(tweetsFile);

		int totalTweets = statuses.size();

		twitter4j.Status myStatus = (twitter4j.Status)statuses.get(0);
		System.out.println(myStatus.getText().replaceAll("\n", " ").replaceAll("\\s+", " ").trim());

		for(int i = 0; i < totalTweets; i++){
			twitter4j.Status tweet = (twitter4j.Status)statuses.get(i);

			String tweetString = tweet.getText().replaceAll("\n", " ").replaceAll("\\s+", " ").trim() + "\n";
			//we don't want retweets
			if(!tweetString.substring(0, 2).equals("RT"))
				sw.write(tweetString);
		}
		sw.close();
	}

}
