import java.util.HashMap;


public class Estimations {

	/**
	 * Helper method to find Laplace smoothed bigram conditional probability
	 * @param bigramMap is the HashMap which has the bigrams from the training corpus and their correspoding frequency
	 * @param wordMap is the HashMap of the individual word tokens and their corresponding frequency
	 * @param x is one of the words
	 * @param y is one of the words
	 * @return the MLE probaility of the bigram
	 */
	public static double laplace(HashMap<String,Double> bigramMap, HashMap<String,Double> wordMap, String x, String y){
		double numerator = 1;
		double denominator = wordMap.size();

		if(bigramMap.get(x+"<"+y) != null){
			numerator += bigramMap.get(x+"<"+y);

			if(wordMap.get(x) != null){
				denominator += wordMap.get(x);
			}
		}

		return (numerator/denominator);
	}

	public static double laplace(HashMap<String,Double> bigramMap, HashMap<String,Double> wordMap, String bigramName){
		if(bigramName.split("[<]").length == 2){
			String x = bigramName.split("[<]")[0];
			String y = bigramName.split("[<]")[1];
			return laplace(bigramMap, wordMap, x, y);
		}
		return laplace(bigramMap, wordMap, bigramName, "");

	}

}
