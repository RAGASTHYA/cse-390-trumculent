import java.util.HashMap;


public class POS_Methods {

	/**
	 *  Parses the sentence from the Tweebo parser POS tagger to build the
	 * appropriate map. The key is the word and the value is it's POS tag
	 * @param sentence is the sentence to be parsed
	 * @param tagsFrequencyMap is the map of all tags and their frequency
	 * @param tagMap is the map of all words with their corresponding tag
	 * @param transitionMap 
	 * @param emissionMap
	 */
	public static void performParsing(String sentence, HashMap<String, Double> tagsFrequencyMap,HashMap<String, String> tagMap, HashMap<String, Double> transitionMap, HashMap<String, Double> emissionMap) {

		sentence = sentence.replaceAll("[\\s]{1,}0[.].*", "").replaceAll("\\s+", " ").trim();

		// split on space to get individual tokens
		String[] tokensInString = sentence.split(" ");
		int totalTokens = tokensInString.length;

		int offset = (totalTokens / 2);
		for (int i = 0; i < offset; i++) {
			//put the Word : Tag
			String word = tokensInString[i];
			String tag = tokensInString[i + offset];

			Processing.putStringInHashMap(tag, tagsFrequencyMap);

			tagMap.putIfAbsent(word, tag);

			//emission Word -> Tag : Frequency
			Processing.putStringInHashMap(tag+"<"+word, emissionMap);
		}

		//getting all the transition tags A -> B : Frequency where A and B are POS tags
		for(int j = offset; j < totalTokens - 1; j++){
			String transitionCouple = tokensInString[j] + "<" + tokensInString[j+1];
			Processing.putStringInHashMap(transitionCouple, transitionMap);
		}
	}

	/**
	 * This method will be used on the user's input sentence
	 * @param sentence
	 * @param tagMap
	 */
	public static void performParsing(String sentence, HashMap<String, String> tagMap){
		sentence = sentence.replaceAll("[\\s]{1,}0[.].*", "").replaceAll("\\s+", " ").trim();

		// split on space to get individual tokens
		String[] tokensInString = sentence.split(" ");
		int totalTokens = tokensInString.length;

		int offset = (totalTokens / 2);
		for (int i = 0; i < offset; i++) {
			//put the Word : Tag
			String word = tokensInString[i];
			String tag = tokensInString[i + offset];

			tagMap.putIfAbsent(word, tag);
		}
	}
}
