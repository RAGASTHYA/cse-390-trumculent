import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;


public class JSONMethods {

	//loads the contents of a JSON file
	public static JsonObject loadJSONFile(String jsonFilePath) throws IOException {
		InputStream is = new FileInputStream(jsonFilePath);
		JsonReader jsonReader = Json.createReader(is);
		JsonObject json = jsonReader.readObject();
		jsonReader.close();
		is.close();
		return json;
	}
	
	/**
	 * Makes the JSON array for a word and it's corresponding frequency
	 * @param map
	 * @param arrayBuilder
	 */
	public static void generateWordFrequencyJsonArray(HashMap<String, Double> map, JsonArrayBuilder arrayBuilder){
		for(String key: map.keySet()){
			JsonObject toAdd = makeWordFrequencyObject(map, key);
			arrayBuilder.add(toAdd);
		}
	}
	
	/**
	 * Builds a JSON object -> Word:Frequency
	 * @param map
	 * @param key
	 * @return
	 */
	public static JsonObject makeWordFrequencyObject(HashMap<String, Double> map, String key){
		JsonObject jso = Json.createObjectBuilder().add(key,map.get(key)).build();
		return jso;
	}
	
	/**
	 * Makes the JSON array for a word and it's corresponding tag
	 * @param map
	 * @param arrayBuilder
	 */
	public static void generateWordTagJsonArray(HashMap<String, String> map, JsonArrayBuilder arrayBuilder){
		for(String key: map.keySet()){
			JsonObject toAdd = makeWordTagObject(map, key);
			arrayBuilder.add(toAdd);
		}
	}
	/**Builds a JSON object -> Word:Tag
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	public static JsonObject makeWordTagObject(HashMap<String, String> map, String key){
		JsonObject jso = Json.createObjectBuilder().add(key,map.get(key)).build();
		return jso;
	}

}
